# Services

## Overview

The `Services` view can be used to monitor the status and manage the configuration
of services used by apps. Services are managed entirely by Cloudron. When apps are deployed,
Cloudron provisions and configures services internally.

You don't have to worry about keeping the services updated - it's part of the Cloudron platform.

<center>
<img src="../img/services.png" class="shadow" width="500px">
</center>

## Configure

To configure a service, use the `Configure` button:

<center>
<img src="../img/services-configure.png" class="shadow" width="500px">
</center>


This will open up a dialog with various settings depending on the service:

<center>
<img src="../img/services-configure-dialog.png" class="shadow" width="500px">
</center>

## Logs

To view the logs of a services, click the `Logs` button:

<center>
<img src="../img/services-logs.png" class="shadow" width="500px">
</center>

