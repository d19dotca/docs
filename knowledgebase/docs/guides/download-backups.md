## Overview

In this guide, we will see how to download backups.

## Backup file names

Backups are categorized at the top level as timestamped directories. This timestamp
indicates the time at which the backup was started. In addition, there is a directory
called `snapshot`. The `snapshot` directory is a working directory for the backup logic
and should not be touched.

Inside each timestamped directory, Cloudron stores the backup of each app individually.
These backups have the file name `app_<location>_<timestamp>_v<package_version>`. When using
the `tgz` backup format, the file will have an extension `.tar.gz`. When using the `rsync`
backup format, this will be a directory.

Email backups are stored with the file name `mail_<timestamp>_v<box_version>`.

Cloudron stores the backup of the platform itself with the file name `box_<timestamp>_v<box_version>`.

## File System

To download a backup when using the `Filesystem` provider, simply use a tool like `scp`.

```
$ scp root@<ip>:/var/backups/2022-04-05-184006-496/app_wekan.cloudron.space_v4.19.2.tar.gz .
```

## S3 (UI)

If you use one of the S3 compatible providers, you can usually go to the UI of the storage provider and find the latest timestamp.
For example, in DO spaces:

<center>
<img src="/img/backups-do-spaces-listing.png" class="shadow" width="500px">
</center>

In the screenshot above, we can see that `2021-03-10-182730-145` is the latest. Navigate inside the directory and download the files.

## S3 (AWS CLI)

In some cases, some S3 providers do not have a UI to the object storage or the UI may simply fail to load with many objects.
We can use the S3 CLI in that case to determine the backup ID.

* First, install the AWS CLI tool. On Linux, you can simply do `sudo apt install awscli`. For other platforms, see [this guide](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv1.html).

* List the objects using the CLI tool. Note that sometimes you have to set the `AWS_DEFAULT_REGION` to be the region name of the provider and sometimes to `us-east-1`. This setting depends on your provider. In the example below, `cloudron-backups2` is the name of the bucket. If you provided a `prefix`, then you must list `s3://cloudron-backups2/prefix` in the example below.

```
$ export AWS_ACCESS_KEY_ID=JWAJUADXZJU2LBPBQLG2
$ export AWS_SECRET_ACCESS_KEY=SomeSecretKeyThatCannotBePublic
$ export AWS_DEFAULT_REGION=us-east-1
$ aws --endpoint-url=https://sfo2.digitaloceanspaces.com s3 ls s3://cloudron-backups2/
                           PRE 2021-03-02-162423-447/
                           PRE 2021-03-02-162651-715/
                           PRE 2021-03-05-072427-396/
                           PRE 2021-03-10-182730-145/
                           PRE snapshot/
```

We can see above that the latest timestamp is `2021-03-10-182730-145`. Now, we can list the contents of that directory.

```
$ aws --endpoint-url=https://sfo2.digitaloceanspaces.com s3 ls s3://cloudron-backups2/2021-03-10-182730-145/
2021-03-10 10:27:33    7129765 app_wekan.cloudron.space_2021-03-10-182732-640_v1.2.0.tar.gz
2021-03-10 10:27:34     422273 box_2021-03-10-182734-919_v6.2.3.tar.gz
```

Use `aws s3 sync` to download the file.

