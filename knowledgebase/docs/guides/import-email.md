# Import email

## Overview

In this guide, we will see how to import existing email from an external email provider to Cloudron Mail.
Importing email works by fetching email via IMAP from the external provider and pushing it to Cloudron
Mail server via IMAP.

Various tools like [imapsync](https://imapsync.lamiral.info), [OfflineIMAP](https://www.offlineimap.org/)
and [isync](https://github.com/gburd/isync) can be used for this purpose. In this guide, we will use imapsync.

## Add domain

The first step is to add the domain in Cloudron and enable IMAP access for the domain on the Cloudron mail server.
To do so, add the domain in `Domain` view. Then in the `Email` view, select the domain and click `Enable` in the
`Incoming` section.

At this point, we only want to import email and do not want Cloudron to start receiving emails for the domain.
For this reason, uncheck the `Setup Mail DNS records now` option when enabling email for the domain. This
will allow you to continue using email with your current provider.

<center>
<img src="/img/mail-enable-no-dns-setup.png" class="shadow" width="500px">
</center>

## Create mailbox

Next, [add](/email/#add) the mailbox(es) that you want to import in Cloudron in the `Email` view.

<center>
<img src="/img/mail-add-mailbox.png" class="shadow" width="500px">
</center>

## Imapsync

imapsync can be download to your PC from [here](https://imapsync.lamiral.info/dist/). imapsync will do an 
incremental and recursive transfer from one mailbox to another.

To import say `founders@cloudron.club`, use something like the following command:

```
imapsync \
    --host1 imap.external.server --user1 founders@cloudron.club --password1 externalmailboxpassword \
    --host2 my.cloudron.club --user2 founders@cloudron.club --password2 cloudronmailboxpassword
```

The above command has to be repeated for each mailbox you have on the external server.

There are various caveats when importing mail from GMail, you can read more [here](https://github.com/imapsync/imapsync/blob/master/FAQ.d/FAQ.Gmail.txt). An example usage for GMail would be:

```
imapsync --gmail1 --user1 founders@cloudron.club --password1 MASKED \
    --host2 my.cloudron.club --user2 founders@cloudron.club --password2 MASKED \
    --maxbytespersecond 20000 --useheader=X-Gmail-Received --useheader Message-Id --automap --regextrans2 s,\[Gmail\].,, --skipcrossduplicates   --folderlast [Gmail]/All Mail --exclude "\[Gmail\]/Spam"
```

## Verify

Once imported, you can verify if the mails look correct on Cloudron using apps like Rainloop or Roundcube.

## Finish

The final step is to switch over the domain's DNS to use Cloudron mail. This can be done by clicking the
`Re-setup DNS` button in the `Email` -> Select domain -> `Status` page.

<center>
<img src="/img/mail-resetup-dns.png" class="shadow" width="500px">
</center>

