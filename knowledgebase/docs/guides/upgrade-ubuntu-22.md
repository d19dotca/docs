# Upgrading to Ubuntu 22.04 (Jammy Jellyfish)

## Overview

In this guide, we will see how to upgrade an existing Ubuntu 20.04 based Cloudron to Ubuntu 22.04.
If you are still on Ubuntu 20, you must first upgrade to Ubuntu 20 before upgrading to Ubuntu 22.
Follow [this guide](/guides/upgrade-ubuntu-20/) to upgrade to Ubuntu 20.04.

Please note that Ubuntu 20.04 will be supported by Canonical till 2024. Cloudron will support
18.04, 20.04 and 22.04. It has the same feature set across all the versions.

## Checklist

Before upgrading, please note the following:

* Cloudron has to be on atleast version 7.2. This can be verified by checking the version in the Settings view. Cloudron releases prior to 7.2.0 do not support Ubuntu 22.04.
* Ubuntu has to be on version 20.04. Check the output of `lsb_release -a` to confirm this.
* The upgrade takes around 1-3 hours based on various factors like network/cpu/disk etc

## Pre-flight

Before starting the upgrade process, it's a good idea to create a server snapshot to rollback quickly. If your VPS does not have snapshotting feature, it's best to create a full Cloudron backup before attempting the upgrade (Backups -> Create Backup now).

## Upgrading

Start the upgrade:

```
# dpkg --configure -a
# apt update
# apt upgrade
# do-release-upgrade -d
```

Upgrade notes:

* Accept running an additional ssh deamon at port 1022
* Choose 'yes' to Restart services without asking.
* For all packages (mime, nginx, timesyncd, logrotate, journald etc), select N or O  : keep your currently-installed version. This is the 'default'.
* `nginx` upgrade might fail towards the end. You can ignore this.

## Post Upgrade

Re-run the start script to complete the upgrade.

```
# systemctl stop box
# /home/yellowtent/box/setup/start.sh
# reboot
```

## Post-update checks

* `lsb_release -a` will output Ubuntu 22.04.
* `systemctl status box` will say `active (running)`.
* `systemctl status collectd` will say `active (running)`.
* Verify that all the services in the Services view of Cloudron dashboard are running.

