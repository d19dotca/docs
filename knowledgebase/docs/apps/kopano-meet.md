# <img src="/img/kopano-meet-logo.png" width="25px"> Kopano Meet App

## TURN Server

The Kopano Meet app is pre-configured to use Cloudron's built-in TURN server. No
additional configuration is required.

