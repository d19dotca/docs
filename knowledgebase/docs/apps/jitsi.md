# <img src="/img/jitsi-logo.png" width="25px"> Jitsi App

## About

Jitsi Meet is an open source JavaScript WebRTC application used primarily for video conferencing.

* Questions? Ask in the [Cloudron Forum - Jitsi](https://forum.cloudron.io/category/139/jitsi)
* [Jitsi Website](https://jitsi.org/)
* [Jitsi docs](https://jitsi.github.io/handbook/docs/intro)
* [Jitsi issue tracker](https://github.com/jitsi/jitsi/issues)
