# <img src="/img/hastebin-logo.png" width="25px"> Hastebin App

## About

Hastebin is an open source pastebin written in node.js.

* Questions? Ask in the [Cloudron Forum - Hastebin](https://forum.cloudron.io/category/78/hastebin)
* [Hastebin Website](https://hastebin.com/about.md)
* [Hastebin issue tracker](https://github.com/seejohnrun/haste-server/issues)

## Deleting pastes

Pastes never expire and have to be cleaned up manually. For this,
use the [File Manager](/apps#file-manager) and simply
remove the pastes stored under `/app/data` as needed.

