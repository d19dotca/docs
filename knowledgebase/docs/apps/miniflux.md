# <img src="/img/miniflux-logo.png" width="25px"> Miniflux App

## About

Miniflux is a minimalist and opinionated feed reader.

* Questions? Ask in the [Cloudron Forum - Miniflux](https://forum.cloudron.io/category/144/miniflux)
* [Miniflux Website](https://miniflux.app/)
* [Miniflux docs](https://miniflux.app/docs/index.html)
* [Miniflux issue tracker](https://github.com/miniflux/v2/issues)

