# <img src="/img/jirafeau-logo.png" width="25px"> Jirafeau App

## About

Jirafeau is a "one-click-filesharing": Select your file, upload, share a link. That's it.

* Questions? Ask in the [Cloudron Forum - Jirafeau](https://forum.cloudron.io/category/121/jirafeau)
* [Jirafeau Website](https://jirafeau.net/)
* [Jirafeau issue tracker](https://gitlab.com/mojo42/Jirafeau/-/issues)

## Customization

The app allows for a multitude of customization by overwriting config values in `/app/data/custom.php` using the [File Manager](/apps/#file-manager).

All options can be seen [here](https://gitlab.com/mojo42/Jirafeau/-/blob/master/lib/config.original.php).

For example settings the page title can be done by adding the following:
```
$cfg['organisation'] = 'My filedrop';
```

## Restricting Uploads

By default, anyone can upload to the instance. You can add customizations in `/app/data/custom.php` to protect
your instance.

To protect uploads with a password:

```
$cfg['upload_password'] = array('psw1');         // One password
```

Alternately, uploads can be restricted by IP address(es):

```
$cfg['upload_ip'] = array('123.45.0.0/16');
```

If you set a password, you can set a list of IPs that can upload without password:

```
$cfg['upload_ip_nopassword'] = array();
```

## Themes

To change the theme, edit `/app/data/custom.php`. List of theme names is [available here](https://gitlab.com/mojo42/Jirafeau/-/tree/master/media).

```
$cfg['style'] = 'dark-courgette';
```

If you want to make a custom theme, then use the [Web Terminal](/apps/#web-terminal) to first copy over
from an existing theme (themes are located under `/app/code/media.original`):

```
# cd /app/data/media
# cp -r /app/code/media.original/dark-courgette/ mydarkhorse
```

Edit the new theme `mydarkhorse` to your content. Then, change `/app/data/custom.php`:

```
$cfg['style'] = 'mydarkhorse';
```

