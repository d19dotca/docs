# <img src="/img/greenlight-logo.png" width="25px"> Greenlight App

## About

Greenlight is a really simple end-user interface for your BigBlueButton server. 

* Questions? Ask in the [Cloudron Forum - Greenlight](https://forum.cloudron.io/category/103/greenlight)
* [Greenlight Website](https://github.com/bigbluebutton/greenlight)
* [Greenlight issue tracker](https://github.com/bigbluebutton/greenlight/issues)

## Installing BigBlueButton

This app is intended to work alongside a BigBlueButton installation. Greenlight is the frontend which
has Cloudron user authentication and the BigBlueButton install is the backend. BBB must be
installed on a separate VM following the instructions at https://github.com/bigbluebutton/bbb-install.

To summarize the installation procedure:

* Click yourself a dedicated bare metal. Let's say https://www.hetzner.com/de/dedicated-rootserver/ax41-nvme
* Install ubuntu 16 on it.
* SSH into it
* `wget -qO- https://ubuntu.bigbluebutton.org/bbb-install.sh | bash -s -- -w -v xenial-22 -s bbb.example.com -e info@example.com -c <hostname>:<secret>`.
* Install Greenlight on Cloudron
* Take the output of `bbb-conf --secret` and put it into `/app/data/.env` of Greenlight using the [File manager](/apps/#file-manager).
* Restart the Greenlight app.

## Creating a new user with a role

To create a new User, open a [Web terminal](/apps/#web-terminal) and run the following commands:

```bash
# Information
# bundle exec rake user:create["name","email","password","role"]

# Creating an admin
bundle exec rake user:create["admin","admin@server.local","changeme","admin"]

# Creating a user - Can be usefull if there is no cloudron user
bundle exec rake user:create["user-noldap","user-noldap@server.local","changeme","user"]
```

For further documentation about user creation you can visit the official documentation. [Greenlight - Creating Accounts](https://docs.bigbluebutton.org/greenlight/gl-admin.html#creating-accounts)

## Customizing looks and content of Greenlight

Please visit the official [Greenlight Documentaion](https://docs.bigbluebutton.org/greenlight/gl-admin.html#site-settings) for information about the customization.

## Customizing the Landing Page

The [Official Documentation](https://docs.bigbluebutton.org/greenlight/gl-customize.html#customizing-the-landing-page).

You can follow the [Official Documentation](https://docs.bigbluebutton.org/greenlight/gl-customize.html#customizing-the-landing-page) with the Cloudron [File manager](/apps/#file-manager).

Just note the path changes:

```
app/views/main/index.html.erb => /app/data/views/index.html.erv
config/locales/en.yml => /app/data/locales/en.yml
```

