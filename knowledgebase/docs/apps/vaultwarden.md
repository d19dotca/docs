# <img src="/img/vaultwarden-logo.png" width="25px"> Vaultwarden App

## About

Bitwarden is an Open Source Password Management solution for individuals, teams, and business organizations.
Vaultwarden is an unofficial Bitwarden compatible server written in Rust, fully compatible with the client apps.

* Questions? Ask in the [Cloudron Forum - Vaultwarden](https://forum.cloudron.io/category/64/bitwardenrs)
* [Vaultwarden Website](https://github.com/dani-garcia/vaultwarden)
* [Vaultwarden issue tracker](https://github.com/dani-garcia/vaultwarden/issues)

## Users

Bitwarden does not support Single Sign On. This is by design for security reasons. You must create a new password for your Bitwarden account.

By default, open registration is enabled. This can be changed via the config variables by editing `/app/data/config.json` using
the [File Manager](/apps/#file-manager). For example, to disable signup but allow invitations set the variables as below:

```
  "signups_allowed": false,
  "invitations_allowed": true,
```

## Admin

The admin UI is located `/admin`. To login, look for the `admin_token` field inside `/app/data/config.json` using the [File manager](/apps/#file-manager).

<center>
<img src="/img/bitwarden-admin.png" class="shadow" width="500px">
</center>

