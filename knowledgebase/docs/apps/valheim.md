# <img src="/img/valheim-logo.png" width="25px"> Valheim App

## About

This app packages the Valheim Dedicated Server

* Questions? Ask in the [Cloudron Forum - Valheim](https://forum.cloudron.io/category/128/valheim)
* [Valheim Game Website](https://www.valheimgame.com/)
