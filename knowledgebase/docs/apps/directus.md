# <img src="/img/directus-logo.png" width="25px"> Directus App

## About

Directus is an Instant App & API for your SQL Database.

* Questions? Ask in the [Cloudron Forum - Directus](https://forum.cloudron.io/category/101/directus)
* [Directus Website](https://directus.io)
* [Directus issue tracker](https://github.com/directus/directus/issues)

## Environment variables

[Custom environment variables](https://github.com/directus/directus/blob/main/api/example.env) can be set
in `/app/data/env` using the [File manager](/apps/#file-manager).

Be sure to restart the app after making any changes.
