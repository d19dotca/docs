# <img src="/img/owncast-logo.png" width="25px"> Owncast App

## About

Owncast is an open source, self-hosted, decentralized, single user live video streaming and chat server for running your own live streams similar in style to the large mainstream options. It offers complete ownership over your content, interface, moderation and audience.

* Questions? Ask in the [Cloudron Forum - Owncast](https://forum.cloudron.io/category/143/owncast)
* [Owncast Website](https://owncast.online/)
* [Owncast docs](https://owncast.online/docs/)
* [Owncast issue tracker](https://github.com/owncast/owncast/issues/)
* [Owncast Rocket.chat](https://owncast.rocket.chat/home)

## Embedding

The stream can be [embedded](https://owncast.online/docs/embed/) like so:

```
<iframe
  src="https://your.host/embed/video"
  title="Owncast"
  height="350px" width="550px"
  referrerpolicy="origin"
  scrolling="no"
  allowfullscreen>
</iframe>
```

## HLS Stream

The HTL Stream is located at `http://your.host/hls/stream.m3u8`

