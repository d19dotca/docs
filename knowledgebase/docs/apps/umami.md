# <img src="/img/umami-logo.png" width="25px"> Umami App

## About

Umami is a simple, fast, privacy-focused alternative to Google Analytics. 

* Questions? Ask in the [Cloudron Forum - Umami](https://forum.cloudron.io/category/141/umami)
* [Umami Website](https://umami.is/)
* [Umami docs](https://umami.is/docs/about)
* [Umami issue tracker](https://github.com/mikecao/umami/issues)


