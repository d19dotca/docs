# <img src="/img/jellyfin-logo.png" width="25px"> Jellyfin App

## About

Jellyfin is the volunteer-built media solution that puts you in control of your media. Stream to any device from your own server, with no strings attached. Your media, your server, your way.

* Questions? Ask in the [Cloudron Forum - Jellyfin](https://forum.cloudron.io/category/85/jellyfin)
* [Jellyfin Website](https://jellyfin.org/)
* [Jellyfin issue tracker](https://github.com/jellyfin/jellyfin/issues/)

