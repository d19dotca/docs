# <img src="/img/uptime-kuma-logo.png" width="25px"> Uptime Kuma App

## About

Uptime Kuma is a self-hosted monitoring tool like "Uptime Robot".

* [Website](https://github.com/louislam/uptime-kuma)
* [Issue tracker](https://github.com/louislam/uptime-kuma/issues)

