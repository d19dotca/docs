# <img src="/img/ghost-logo.png" width="25px"> Ghost App

## About

Ghost makes it simple to publish content online, grow an audience with email newsletters, and make money from premium memberships.

* Questions? Ask in the [Cloudron Forum - Ghost](https://forum.cloudron.io/category/59/ghost)
* [Ghost Website](https://ghost.org/)
* [Ghost forum](https://forum.ghost.org/)
* [Ghost issue tracker](https://github.com/TryGhost/Ghost/issues)

## Structured data

Ghost outputs basic meta tags to allow rich snippets of your content to be recognised by popular social networks.
Currently there are 3 supported rich data protocols which are output in `{{ghost_head}}`:

- Schema.org - http://schema.org/docs/documents.html
- Open Graph - http://ogp.me/
- Twitter cards - https://dev.twitter.com/cards/overview

The Cloudron app enables output of [structured data](https://github.com/TryGhost/Ghost/blob/master/PRIVACY.md#structured-data)
by default.

## Gravatar

For [privacy](https://github.com/TryGhost/Ghost/blob/master/PRIVACY.md) reasons, Gravatar functionality is disabled
by default. You can re-enable this by editing the `useGravatar` field in `/app/data/config.production.json` using
the  [File Manager](/apps#file-manager). Be sure to restart the app after editing the config file.

## Importing

You can import content from another Ghost installation from Settings -> Labs -> Import content.

If the JSON file is large, the import might fail. To fix this:

* Give the app [more memory](/apps/#memory-limit) (say 2GB).
* Next, use the [File Manager](/apps#file-manager) to edit `/app/data/env` to adjust the NodeJS `max-old-space-size` limit.
  Set it to 2GB using a line like this `export NODE_OPTIONS="--max-old-space-size=2048"`
* Restart Ghost and try the import again.

## Subscribe Button

The `Subscribe` button lets uses subscribe to your blog. If you don't use this feature, you can disable the functionality
as follows:

* `Settings` -> `Membership` -> change `Subscription access` to `Nobody`. This will remove the floating Subscribe button
  at the bottom of the page. You can also remove the button from the Portal using `Settings` -> `Membership` -> `Customize Portal`.

* To remove the Subscribe button on the nav bar, you have to adjust the theme or inject custom CSS as suggested [here](https://github.com/TryGhost/Casper/issues/775#issuecomment-801728321).

## Email

Ghost sends two types of emails:

* Transactional emails - used for password reset, member sign up confirmation, invites etc.
* Bulk emails - for newsletters. This [requires](https://ghost.org/docs/faq/mailgun-newsletters/) Mailgun.

### Transactional emails

Like all apps, Ghost sends out emails via Cloudron's mail server. You can setup
a [relay](/email/#relay-outbound-mails) in Cloudron's Email view to configure how these mails are ultimately sent out.

There are three email addresses in Ghost:

* Password Reset & Invitation - This address can be changed using the Mail FROM address set in the [Email section](/apps/#mail-from-address) of the app.

* Support email address - defaults to `noreply@app.example.com` . This can be changed in `Membership` -> `Portal Settings` -> `Customize Portal` -> `Account page settings` . See the notes below on how to change this.

* Newsletter email address - defaults to `noreply@app.example.com` . This can be changed in `Email newsletter` -> `Newsletter` -> `Customize` -> `Email addresses` -> `Sender email address`. See the notes below on how to change this.

When trying to change the Support and Newsletter email addresses, to say `newaddress@example.com`, Ghost will send a confirmation mail from `noreply@example.com` to `newaddress@example.com`. The `noreply@` address is [hardcoded](https://github.com/TryGhost/Ghost/blob/main/core/server/services/members/settings.js#L76) and cannot be changed.

Out of the box, Cloudron will block this confirmation mail because it does not allow apps to send emails with arbitrary addresses. To remedy this:

* In Cloudron dashboard, go to Email -> Select Domain -> Settings -> [Enable Masquerading](/email/#disable-from-address-validation). This will allow Ghost to send emails with `noreply@example.com`. Do not turn off this setting later because Ghost will need to send email as the Support email address for subscription confirmation.

* Create the `support@` and `newsletter@` mailboxes in `example.com`. This is required to receive the confirmation mails.

* Change the address inside Ghost. Click on the received confirmation links.

See [this forum post](https://forum.cloudron.io/topic/7043/how-on-earth-do-you-get-ghost-memberships-and-newsletters-working-with-cloudron/20) for a step by step guide.

### Bulk emails

Newsletters are sent out to members using Mailgun. Mailgun is [required](https://ghost.org/docs/faq/mailgun-newsletters/).
You can sign up with Mailgun and setup the API Key at `Settings` -> `Email newsletter`.

