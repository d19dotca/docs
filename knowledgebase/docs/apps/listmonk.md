# <img src="/img/listmonk-logo.png" width="25px"> Listmonk App

## About

Listmonk is a standalone, self-hosted, newsletter and mailing list manager. It is fast, feature-rich, and packed into a single binary.

* Questions? Ask in the [Cloudron Forum - Listmonk](https://forum.cloudron.io/category/145/listmonk)
* [Listmonk Website](https://listmonk.app/)
* [Listmonk issue tracker](https://github.com/knadh/listmonk/issues/)

## Login

The login credentials are stored in `/app/data/env.sh`. Please edit the following section:

```
export LISTMONK_app__admin_username=admin
export LISTMONK_app__admin_password=changeme
```

Be sure to restart the app, after making any changes.

## Timezone

To set the timezone, edit the `TZ` variable inside `/app/data/env.sh` using the [File manager](/apps/#file-manager).

Be sure to restart the app, after making any changes.

