# <img src="/img/mastodon-logo.png" width="25px"> Mastodon App

## About

Mastodon is an open source decentralized social network - by the people for the people. Join the federation and take back control of your social media!

* Questions? Ask in the [Cloudron Forum - Mastodon](https://forum.cloudron.io/category/41/mastodon)
* [Mastodon Website](https://joinmastodon.org/)
* [Mastodon issue tracker](https://github.com/tootsuite/mastodon/issues/)

## Admin

To make a user an administrator, use the [Web Terminal](/apps/#web-terminal)
and run the following command:

```
    bin/tootctl accounts modify <username> --role admin
```

## Adding users

When used with Cloudron authentication, simply add new users to the Cloudron dashboard.

Without Cloudron authentication, new users can be added using the CLI:

```
    bin/tootctl accounts create testusername --email=test@cloudron.io
```

## Username restriction

Usernames can contain only letters, numbers and underscores. On Cloudron, usernames can be
created with hyphen and dot. Such usernames cannot login to Mastodon and will get a "HTTP ERROR 422" when
attempting to login. Please see the [upstream bugreport](https://github.com/mastodon/mastodon/issues/12020) for more information.

## Registration

Registration is closed by default. To enable, login to Mastodon as an admin and change the "Registration mode"
under `Administration` -> `Site Settings`.

## Federation

Cloudron will setup Mastodon accounts to be of the form `username@social.example.org` when you install
Mastodon at `social.example.org`. This domain is called the `LOCAL_DOMAIN` in Mastodon terminology.

Changing the `LOCAL_DOMAIN` will let you have handles as `username@example.org` even when installed at `social.example.org`
Changing the `LOCAL_DOMAIN` is not recommended since it is complicated and in most cases unnecessary.
This is because Mastodon account names are not intended to be remembered like usernames (it's not like email where
you can start following another account). Instead, users usually visit a website and click the 'Follow' button.

If you decide to not change the `LOCAL_DOMAIN`, no further configuration is required and your Mastodon instance
is already set up for federation.

### Changing LOCAL_DOMAIN

You can change the account domain name by using the [File Manager](/apps/#file-manager)
and changing `LOCAL_DOMAIN` in `/app/data/env.production`. After that, you have to configure `LOCAL_DOMAIN`'s
web server to serve up `.well-known/host-meta` query.

If `LOCAL_DOMAIN` is an app on Cloudron, you can use Cloudron's Well Known URI
support. Go to the `Domains` view and set the Mastodon domain in the `Advanced` settings:

<center>
<img src="/img/mastodon-wellknown.png" class="shadow">
</center>

If the `LOCAL_DOMAIN` is **NOT** hosted on Cloudron, you must figure out a suitable way to serve up the well-known documents.
Here are some hints:

* For WordPress, you can setup a redirect using [Redirection plugin](https://wordpress.org/plugins/redirection/)
* For Ghost,you can add a [redirects.json](https://ghost.org/tutorials/implementing-redirects/)
* For Surfer, simply upload the XML above into `.well-known/host-meta`.
* For anything else, setup nginx config as follows:

```
location = /.well-known/host-meta {
       return 301 https://social.example.org$request_uri;
}
```

## Following users

To follow external users, visit their mastodon account and click on 'Follow'. This will popup a window asking your
mastodon identity (which will be `username@LOCAL_DOMAIN`).

If you have an existing account on another server, you can bring those connections with you to your own server.
For this, go to Settings -> Data Export and download your following list as a CSV file, and finally
on your own server, you go to Settings -> Import and upload that file.

## Scaling

Various parameters in Puma, Streaming and Sidekiq can be fine-tuned for concurrency. To change the
parameters, edit `/app/data/config.sh` using the [File manager](/apps/#file-manager) and restart the app.
See [scaling docs](https://docs.joinmastodon.org/admin/scaling/) for more information.

