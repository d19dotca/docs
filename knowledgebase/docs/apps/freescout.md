# <img src="/img/freescout-logo.png" width="25px"> FreeScout App

## About

FreeScout is the super lightweight free open source help desk and shared inbox.

* Questions? Ask in the [Cloudron Forum - FreeScout](https://forum.cloudron.io/category/68/freescout)
* [FreeScout Website](https://freescout.net)
* [FreeScout issue tracker](https://github.com/freescout-helpdesk/freescout/issues)

## Mailbox Setup

Mailboxes do not need to be hosted on Cloudron itself. The app acts as a regular email client and thus can be setup for any IMAP mailbox.
For sending emails of a specific mailbox, the STMP method has to be selected as `php mail()` or `sendmail` wont work on Cloudron.

<center>
<img src="/img/freescout-smtp-settings.png" class="shadow">
</center>

### Cloudron mailbox

To configure Freescout with a Cloudron mailbox, use the following connection settings.

For Sending Emails:

!!! note "Set encryption to None if FreeScout and Cloudron Mail are on same server"
    For technical reasons, the mail server does not offer encryption for apps hosted on the same server.
    For this reason, set the Encryption in the screenhot below to None instead of TLS. This is safe since
    all communication is internal to the server.

<center>
<img src="/img/freescout-sending-emails.png" class="shadow">
</center>

For Fetching Emails, use the configuration below. Unlike sending emails, the encryption setting is always `SSL`.

<center>
<img src="/img/freescout-fetching-emails.png" class="shadow">
</center>


## Modules

FreeScout supports a wide variety of modules to extend the app. Most of them can simply be installed through the FreeScout user interface,
however some require additional steps to migrate the database before being used.

To ensure all necessary steps are run after a module installation, simply restart the app through the Cloudron dashboard after activating the module.

## Reset admin password

There is no CLI command to reset the admin password. Instead, just create a new temporary admin
and then reset the password of the original admin.

Using a [Web terminal](/apps/#web-terminal) run the following command:

```
sudo -E -u www-data php artisan freescout:create-user --role admin --firstName Second --lastName Admin --email admin2@cloudron.local --password changeme123 --no-interaction
```

