# <img src="/img/osticket-logo.png" width="25px"> osTicket App

## About

osTicket is the world’s most popular customer support software.

* Questions? Ask in the [Cloudron Forum - osTicket](https://forum.cloudron.io/category/89/osticket)
* [osTicket Website](https://osticket.com/)
* [osTicket forum](https://forum.osticket.com/)
* [osTicket issue tracker](https://github.com/osTicket/osTicket/issues)

## Admin Checklist

* Do not remove the email address of `osTicket Alerts` under 'Email Addresses'. This mailbox is managed
by Cloudron. The email address can be changed from Cloudron dashboard's [Email section](/apps/#mail-from-address).
To change other settings like the `Email Name`, the osTicket UI requires the SMTP password. You can get this
by opening a [Web Terminal](/apps/#web-terminal) and executing `env | sed -n -e 's/CLOUDRON_MAIL_SMTP_PASSWORD=//p'`.

* Change the administrator email under `Emails` -> `Email Settings and Options`. If you miss this,
  osTicket will send alerts to this address and bounces get attached to tickets.

## User Management

osTicket is integrated with Cloudron user management. However, osTicket does not support
auto creation of user accounts - see [this](https://forum.osticket.com/d/99277-auto-create-user-accounts)
and [this](https://github.com/osTicket/osTicket/issues/1062) for more information.

To workaround agents must be manually added into osTicket before they can login. When adding an agent, choose
LDAP as the authentication backend.

<center>
<img src="/img/osticket-add-agent.png" class="shadow">
</center>

## Emails

osTicket can be configured to process emails from mailboxes hosted with or without Cloudron.

When the mailbox is hosted in Cloudron, you can use the IMAP+SSL at port 993 for receiving Email:

<center>
<img src="/img/osticket-imap.png" class="shadow">
</center>

To send email, use port 587:

<center>
<img src="/img/osticket-smtp.png" class="shadow">
</center>

## CLI

osTicket comes with a CLI tool for various administrative tasks like managing users.
Use the [Web Terminal](/apps#web-terminal) to run the following command:

```
    sudo -E -u www-data php /app/code/upload/manage.php
```

