# <img src="/img/metabase-logo.png" width="25px"> Metabase App

## About

Metabase is the simplest, fastest way to get business intelligence and analytics to everyone in your company.

* Questions? Ask in the [Cloudron Forum - Metabase](https://forum.cloudron.io/category/86/metabase)
* [Metabase Website](https://www.metabase.com)
* [Metabase forum](https://discourse.metabase.com/)
* [Metabase issue tracker](https://github.com/metabase/metabase/issues)

