# <img src="/img/simple-torrent-logo.png" width="25px"> Simple Torrent App

## About

Simple Torrent is a self-hosted remote torrent client (rebranded from Cloud Torrent).

* Questions? Ask in the [Cloudron Forum - Simple Torrent](https://forum.cloudron.io/category/73/simple-torrent)
* [Simple Torrent Website](https://github.com/boypt/simple-torrent)
* [Simple Torrent issue tracker](https://github.com/boypt/simple-torrent/issues)

## Customization

Use the [File Manager](/apps#file-manager) and edit `cloud-torrent.yaml` for customization.
See the [docs](https://github.com/boypt/simple-torrent/wiki/Config-File) on what can be customized.
Some important fields are:

* `downloaddirectory` - where the download files are placed.
* `donecmd` - a script to run once file has downloaded. See [DoneCmdUsage](https://github.com/boypt/simple-torrent/wiki/DoneCmdUsage)
  for more information.

