# <img src="/img/peertube-logo.png" width="25px"> PeerTube App

## About

PeerTube is an activityPub-federated video streaming platform using P2P directly in your web browser.

* Questions? Ask in the [Cloudron Forum - PeerTube](https://forum.cloudron.io/category/91/peertube)
* [PeerTube Website](https://joinpeertube.org/)
* [PeerTube issue tracker](https://github.com/Chocobozzz/PeerTube/issues)

## Customization

Use the [File manager](/apps#file-manager) to edit custom configuration
under `/app/data/production.yaml`.

## CLI

The CLI can be accessed using the `peertube` command.

### Uploading video

```
# peertube up --file /tmp/video.wmv --url https://peertube.cloudron.club --username root --password changeme --video-name "Sample video"
Uploading Sample video video...
Video Sample video uploaded.
```

## Importing video

```
# peertube import --url https://peertube.cloudron.club --username root --password changeme --target-url https://www.youtube.com/watch?v=xxx --tmpdir /tmp
info: Will download and upload 1 videos.
```

