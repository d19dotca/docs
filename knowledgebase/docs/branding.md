# Branding

The `Branding` view can be used to customize various aspects of the Cloudron like
it's name, logo and footer. The `Branding` view is only accessible by a Cloudron Owner.

## Cloudron Name

The Cloudron name in the `Branding` view. The name is used in the following places:

* Email templates - user invitations and notifications
* Dashboard header/navbar
* Login page

<center>
<img src="/img/branding-name.png" class="shadow" width="500px">
</center>

## Cloudron Avatar

The Cloudron avatar can be changed by clicking on the logo in the `Branding` view. The
avatar is used in the following places:

* Email templates - user invitations and notifications
* Dashboard header/navbar
* Login page

<center>
<img src="/img/branding-name.png" class="shadow" width="500px">
</center>

## Custom Pages

Custom pages can be setup under `/home/yellowtent/boxdata/custom_pages`.

| File  | Description  |
|---|---|
| `app_not_responding.html` | When any app becomes unresponsive |
| `notfound.html`           | Used when the user navigates with the server's IP address. This is also used if you set the DNS of an arbitrary domain to the server with no associated app. |

## Footer

The footer can be branded with [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
from the `Branding` page.

The footer can be templated using the following variables:

* `%YEAR%` - the current year
* `%VERSION%` - the current Cloudron version

<center>
<img src="/img/branding-footer.png" class="shadow" width="500px">
</center>

